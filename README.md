Please find scripts to take EC2 snapshots.

What Script Does:
1.	Determines the instance ID of the EC2 server on which the script runs
2.	Gathers a list of all volume IDs attached to that instance
3.	Take a snapshot of each attached volume
4.	Copy tags to snapshots from volume
5.	The script will then delete all associated snapshots taken by the script that are older than 7 days (configurable)

Pre-requisites:
1.	Volume needs to be tagged properly. Alteast below tags should be created in volume.
CREATED_BY
CREATION_DATE
2.	Create below rags in volume for easy identification
CREATED_BY      - 
CREATION_DATE              -
ENCRYPTION_STATUS    - ENCRYPTED/UNENCRYPTED
ENVIRONMENT - PRODUCTION
HOSTNAME – Hostname of server
MOUNTED_ON - 
NAME or Name- 
PROJECT – 

Permissions – A role should be attached in server with at least below policies. 

{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1495720757000",
            "Effect": "Allow",
            "Action": [
                "ec2:CreateSnapshot",
                "ec2:CreateTags",
                "ec2:DescribeTags",
                "ec2:DeleteSnapshot",
                "ec2:DescribeSnapshots",
                "ec2:DescribeVolumes"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}

Please take care of below
1.	It’s a bash script. Run with bash or full path. Sh will not work.
2.	If you are scheduling the script in manual snapshot. Schedule it at different times at different nodes so that the snapshot taking limit for the account is not crossed.

Please let me know if any queries.

